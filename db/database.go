package db

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Database mongo-driver database
type Database struct {
	database *mongo.Database
}

// CollectionNames 返回数据库中存在的所有集合名称
func (d *Database) CollectionNames() (names []string, err error) {
	names, err = d.database.ListCollectionNames(context.TODO(), options.ListCollectionsOptions{})
	return
}

// C 返回集合
func (d *Database) C(collection string) *Collection {
	return &Collection{collection: d.database.Collection(collection)}
}
