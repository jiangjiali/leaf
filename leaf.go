package leaf

import (
	"gitee.com/jiangjiali/leaf/conf"
	"gitee.com/jiangjiali/leaf/db"
	"gitee.com/jiangjiali/leaf/log"
	"gitee.com/jiangjiali/leaf/module"
	"os"
	"os/signal"
)

const version = "1.1.43"

func Run(mods ...module.Module) {
	// 日志
	if conf.LogLevel != "" {
		logger, err := log.New(conf.LogLevel, conf.LogPath, conf.LogFlag)
		if err != nil {
			panic(any(err))
		}
		log.Export(logger)
		defer logger.Close()
	}

	log.Debug("框架版::SDK v%v", version)
	log.Release("服务器::Run::正在启动")

	// 初始化一个数据连接给服务器使用
	db.Init()

	// 初始化模块
	for i := 0; i < len(mods); i++ {
		module.Register(mods[i])
	}
	module.Init()

	// 关闭服务
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)
	sig := <-c
	log.Release("服务器关闭 (signal: %v)", sig)
	db.Disconnect()
	module.Destroy()
}
