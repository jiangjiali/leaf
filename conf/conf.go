package conf

var (
	LenStackBuf = 4096

	LogLevel string // 日志等级
	LogPath  string // 日志路径
	LogFlag  int    // 日志标志

	IsEncode bool   // 传输数据是否加密
	SM4Key   string // SM4加密钥匙

	HeartbeatInterval int // 心跳时间

	MongoAddr string // mongo数据库连接地址
	MongoPool uint64 // mongo 连接池数量
)
